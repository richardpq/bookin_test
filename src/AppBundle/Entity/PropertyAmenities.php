<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class PropertyAmenities
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int quantity
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="propertyAmenities", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id", nullable=false)
     *
     * @var Property $property
     */
    protected $property;

    /**
     * @ORM\ManyToOne(targetEntity="Amenity", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="amenity_id", referencedColumnName="id", nullable=false)
     *
     * @var Amenity $amenity
     */
    protected $amenity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PropertyAmenities
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set property
     *
     * @param Property $property
     *
     * @return PropertyAmenities
     */
    public function setProperty(Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set amenity
     *
     * @param Amenity $amenity
     *
     * @return PropertyAmenities
     */
    public function setAmenity(Amenity $amenity)
    {
        $this->amenity = $amenity;

        return $this;
    }

    /**
     * Get amenity
     *
     * @return Amenity
     */
    public function getAmenity()
    {
        return $this->amenity;
    }
}
