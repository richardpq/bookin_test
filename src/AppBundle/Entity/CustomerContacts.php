<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class CustomerContacts
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string $contact
     */
    protected $contact;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="customerContacts")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false)
     *
     * @var
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="ContactType")
     * @ORM\JoinColumn(name="contact_type_id", referencedColumnName="id", nullable=false)
     *
     * @var
     */
    protected $contactType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return CustomerContacts
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return CustomerContacts
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set contactType
     *
     * @param ContactType $contactType
     *
     * @return CustomerContacts
     */
    public function setContactType(ContactType $contactType)
    {
        $this->contactType = $contactType;

        return $this;
    }

    /**
     * Get contactType
     *
     * @return ContactType
     */
    public function getContactType()
    {
        return $this->contactType;
    }
}
