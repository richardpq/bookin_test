<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class Customer extends Person
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="customers")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=false)
     *
     * @var Address $address
     */
    protected $address;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="customer")
     *
     * @var ArrayCollection $bookings
     */
    protected $bookings;

    /**
     * @ORM\OneToMany(targetEntity="CustomerContacts", mappedBy="customer", cascade={"remove"})
     *
     * @var ArrayCollection $customerContacts
     */
    protected $customerContacts;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Customer constructor.
     *
     * @param null $firstName
     * @param null $lastName
     */
    public function __construct($firstName = null, $lastName = null)
    {
        parent::__construct($firstName, $lastName);
        $this->bookings = new ArrayCollection();
        $this->customerContacts = new ArrayCollection();
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return Customer
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add booking
     *
     * @param \AppBundle\Entity\Booking $booking
     *
     * @return Customer
     */
    public function addBooking(Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \AppBundle\Entity\Booking $booking
     */
    public function removeBooking(Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Add customerContact
     *
     * @param CustomerContacts $customerContact
     *
     * @return Customer
     */
    public function addCustomerContact(CustomerContacts $customerContact)
    {
        $this->customerContacts[] = $customerContact;

        return $this;
    }

    /**
     * Remove customerContact
     *
     * @param CustomerContacts $customerContact
     */
    public function removeCustomerContact(CustomerContacts $customerContact)
    {
        $this->customerContacts->removeElement($customerContact);
    }

    /**
     * Get customerContacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerContacts()
    {
        return $this->customerContacts;
    }
}
