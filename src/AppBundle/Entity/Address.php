<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 * @ORM\Table
 */
class Address
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     *
     * @var string $address
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=6)
     *
     * @var string $zipCode
     */
    protected $zipCode;

    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $state;

    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $country;

    /**
     * @ORM\OneToMany(targetEntity="Property", mappedBy="address")
     *
     * @var ArrayCollection $properties
     */
    protected $properties;

    /**
     * @ORM\OneToMany(targetEntity="Customer", mappedBy="address")
     *
     * @var ArrayCollection $customers
     */
    protected $customers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->customers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->address."\n".$this->city.', '.$this->state.' '.$this->zipCode."\n".$this->country;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Address
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     *
     * @return Address
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return Address
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Add property
     *
     * @param Property $property
     *
     * @return Address
     */
    public function addProperty(Property $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param Property $property
     */
    public function removeProperty(Property $property)
    {
        $this->properties->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Add customer
     *
     * @param Customer $customer
     *
     * @return Address
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param Customer $customer
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }
}
