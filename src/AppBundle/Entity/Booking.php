<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class Booking
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     *
     * @var \DateTime $checkIn
     */
    protected $checkIn;

    /**
     * @ORM\Column(type="date")
     *
     * @var \DateTime $checkOut
     */
    protected $checkOut;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=3)
     *
     * @var string $ratePerNight
     */
    protected $ratePerNight;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime|null $customerArrivedDate
     */
    protected $customerArrivedDate = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime|null $customerDepartedDate
     */
    protected $customerDepartedDate = null;

    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="bookings")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id", nullable=false)
     *
     * @var Property $property
     */
    protected $property;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="bookings")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false)
     *
     * @var Customer $customer
     */
    protected $customer;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->property."[".$this->checkIn->format('Y-m-d').','.$this->checkOut->format('Y-m-d')."]";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checkIn
     *
     * @param \DateTime $checkIn
     *
     * @return Booking
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    /**
     * Get checkIn
     *
     * @return \DateTime
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * Set checkOut
     *
     * @param \DateTime $checkOut
     *
     * @return Booking
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    /**
     * Get checkOut
     *
     * @return \DateTime
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * Set ratePerNight
     *
     * @param string $ratePerNight
     *
     * @return Booking
     */
    public function setRatePerNight($ratePerNight)
    {
        $this->ratePerNight = $ratePerNight;

        return $this;
    }

    /**
     * Get ratePerNight
     *
     * @return float
     */
    public function getRatePerNight()
    {
        return floatval($this->ratePerNight);
    }

    /**
     * Set customerArrivedDate
     *
     * @param \DateTime $customerArrivedDate
     *
     * @return Booking
     */
    public function setCustomerArrivedDate($customerArrivedDate)
    {
        $this->customerArrivedDate = $customerArrivedDate;

        return $this;
    }

    /**
     * Get customerArrivedDate
     *
     * @return \DateTime
     */
    public function getCustomerArrivedDate()
    {
        return $this->customerArrivedDate;
    }

    /**
     * Set customerDepartedDate
     *
     * @param \DateTime $customerDepartedDate
     *
     * @return Booking
     */
    public function setCustomerDepartedDate($customerDepartedDate)
    {
        $this->customerDepartedDate = $customerDepartedDate;

        return $this;
    }

    /**
     * Get customerDepartedDate
     *
     * @return \DateTime
     */
    public function getCustomerDepartedDate()
    {
        return $this->customerDepartedDate;
    }

    /**
     * Set property
     *
     * @param Property $property
     *
     * @return Booking
     */
    public function setProperty(Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return Booking
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getTotalBookingDays()
    {
        if ($this->checkIn && $this->checkOut) {
            $diff = $this->checkIn->diff($this->checkOut);

            return (int) $diff->format("%a%");
        }

        throw new \Exception('Check-in and/or Check-out are not set');
    }

    /**
     * @return float
     * @throws \Exception
     */
    public function getTotalAmountBooking()
    {
        return $this->getRatePerNight() * $this->getTotalBookingDays();
    }
}
