<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropertyRepository")
 * @ORM\Table
 */
class Property
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=90)
     *
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     *
     * @var string $description
     */
    protected $description = null;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int $maxPeople
     */
    protected $maxPeople;

    /**
     * @ORM\Column(type="time")
     *
     * @var \DateTime $checkIn
     */
    protected $checkIn;

    /**
     * @ORM\Column(type="time")
     *
     * @var \DateTime $checkOut
     */
    protected $checkOut;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @var int $parkingSpots
     */
    protected $parkingSpots = null;

    /**
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="properties")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=false)
     *
     * @var Address $address
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyType", inversedBy="properties")
     * @ORM\JoinColumn(name="property_type_id", referencedColumnName="id", nullable=false)
     *
     * @var PropertyType $propertyType
     */
    protected $propertyType;

    /**
     * @ORM\OneToMany(targetEntity="PropertyRooms", mappedBy="property", cascade={"remove"})
     *
     * @var ArrayCollection $propertyRooms
     */
    protected $propertyRooms;

    /**
     * @ORM\OneToMany(targetEntity="PropertyAmenities", mappedBy="property", cascade={"remove"})
     *
     * @var ArrayCollection $propertyRooms
     */
    protected $propertyAmenities;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="property")
     *
     * @var ArrayCollection $bookings
     */
    protected $bookings;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->propertyRooms = new ArrayCollection();
        $this->propertyAmenities = new ArrayCollection();
        $this->bookings= new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Property
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Property
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxPeople()
    {
        return $this->maxPeople;
    }

    /**
     * @param int $maxPeople
     *
     * @return Property
     */
    public function setMaxPeople($maxPeople)
    {
        $this->maxPeople = $maxPeople;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param \DateTime $checkIn
     *
     * @return Property
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param \DateTime $checkOut
     *
     * @return Property
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    /**
     * @return int
     */
    public function getParkingSpots()
    {
        return $this->parkingSpots;
    }

    /**
     * @param int $parkingSpots
     *
     * @return Property
     */
    public function setParkingSpots($parkingSpots)
    {
        $this->parkingSpots = $parkingSpots;

        return $this;
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return Property
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set propertyType
     *
     * @param PropertyType $propertyType
     *
     * @return Property
     */
    public function setPropertyType(PropertyType $propertyType)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType
     *
     * @return PropertyType
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }


    /**
     * Add propertyRoom
     *
     * @param PropertyRooms $propertyRoom
     *
     * @return Property
     */
    public function addPropertyRoom(PropertyRooms $propertyRoom)
    {
        $this->propertyRooms[] = $propertyRoom;

        return $this;
    }

    /**
     * Remove propertyRoom
     *
     * @param PropertyRooms $propertyRoom
     */
    public function removePropertyRoom(PropertyRooms $propertyRoom)
    {
        $this->propertyRooms->removeElement($propertyRoom);
    }

    /**
     * Get propertyRooms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyRooms()
    {
        return $this->propertyRooms;
    }

    /**
     * Add propertyAmenity
     *
     * @param PropertyAmenities $propertyAmenity
     *
     * @return Property
     */
    public function addPropertyAmenity(PropertyAmenities $propertyAmenity)
    {
        $this->propertyAmenities[] = $propertyAmenity;

        return $this;
    }

    /**
     * Remove propertyAmenity
     *
     * @param PropertyAmenities $propertyAmenity
     */
    public function removePropertyAmenity(PropertyAmenities $propertyAmenity)
    {
        $this->propertyAmenities->removeElement($propertyAmenity);
    }

    /**
     * Get propertyAmenities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyAmenities()
    {
        return $this->propertyAmenities;
    }

    /**
     * Add booking
     *
     * @param \AppBundle\Entity\Booking $booking
     *
     * @return Property
     */
    public function addBooking(\AppBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \AppBundle\Entity\Booking $booking
     */
    public function removeBooking(\AppBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }
}
