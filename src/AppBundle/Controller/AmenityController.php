<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Amenity;

/**
 * Amenity controller.
 *
 * @Route("/amenity")
 */
class AmenityController extends Controller
{
    /**
     * Lists all Amenity entities.
     *
     * @Route("/", name="amenity_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $amenities = $em->getRepository('AppBundle:Amenity')->getAll();

        return $this->render('amenity/index.html.twig', array(
            'amenities' => $amenities,
        ));
    }

    /**
     * Creates a new Amenity entity.
     *
     * @Route("/new", name="amenity_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        $amenity = new Amenity();
        $form = $this->createForm('AppBundle\Form\Type\AmenityType', $amenity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($amenity);
            $em->flush();

            return $this->redirectToRoute('amenity_index');
        }

        return $this->render('amenity/new.html.twig', array(
            'amenity' => $amenity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Amenity entity.
     *
     * @Route("/{id}/edit", name="amenity_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Amenity $amenity
     *
     * @return Response
     */
    public function editAction(Request $request, Amenity $amenity)
    {
        $editForm = $this->createForm('AppBundle\Form\Type\AmenityType', $amenity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($amenity);
            $em->flush();

            return $this->redirectToRoute('amenity_edit', array('id' => $amenity->getId()));
        }

        return $this->render('amenity/edit.html.twig', array(
            'amenity' => $amenity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Amenity entity.
     *
     * @Route("/{id}", name="amenity_delete", options={"expose"=true})
     * @Method("POST")
     *
     * @param Request $request
     * @param Amenity $amenity
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, Amenity $amenity)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($amenity);
            $em->flush();

            return new JsonResponse('deleted', 200);
        }

        return new JsonResponse('Internal Sever Error', 500);
    }
}
