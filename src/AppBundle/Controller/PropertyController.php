<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\Amenity;
use AppBundle\Entity\PropertyAmenities;
use AppBundle\Entity\PropertyRooms;
use AppBundle\Entity\Room;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Property;
use Symfony\Component\HttpFoundation\Response;

/**
 * Property controller.
 *
 * @Route("/property")
 */
class PropertyController extends Controller
{
    /**
     * Lists all Property entities.
     *
     * @Route("/", name="property_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $properties = $em->getRepository('AppBundle:Property')->findAll();

        return $this->render('property/index.html.twig', array(
            'properties' => $properties,
        ));
    }

    /**
     * Creates a new Property entity.
     *
     * @Route("/new", name="property_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        //TODO: create handler for address
        //TODO: create function for handle Room and amenities
        $property = new Property();
        $form = $this->createForm('AppBundle\Form\Type\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->get('address')->getData();
            $manager = $this->getDoctrine()->getManager();

            if ($address === null && $form->get('propertyAddress')) {
                $address = $form->get('propertyAddress')->getData();

                $manager->persist($address);
                $manager->flush();
            }

            if ($address) {
                $property->setAddress($address);

                $manager->persist($property);
                $manager->flush();
            }

            $roomQuantities = $request->request->get('property')['rooms'];
            $amenityQuantities = $request->request->get('property')['amenities'];

            $this->addNewAmenities($amenityQuantities, $property);
            $this->addNewRooms($roomQuantities, $property);

            $manager->flush();

            return $this->redirectToRoute('property_show', array('id' => $property->getId()));
        }

        return $this->render('property/new.html.twig', array(
            'property' => $property,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Property entity.
     *
     * @Route("/{id}", name="property_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        //$property = $manager->getRepository('AppBundle:Property')->find($id);
        $property = $manager->getRepository('AppBundle:Property')->getPropertyById($id);

        return $this->render('property/show.html.twig', array(
            'property' => $property
        ));
    }

    /**
     * Displays a form to edit an existing Property entity.
     *
     * @Route("/{id}/edit", name="property_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request  $request
     * @param Property $property
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, Property $property)
    {
        //TODO: create handler for address
        //TODO: create function for handle contact details
        $manager = $this->getDoctrine()->getManager();
        $address = $manager->getRepository('AppBundle:Address')->find($property->getAddress());
        $manager->initializeObject($address);
        $editForm = $this->createForm('AppBundle\Form\Type\PropertyType', $property, ['address' => $address]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $addressActual = $editForm->get('address')->getData();
            $addressUpdated = $editForm->get('propertyAddress')->getData();

            $this->updateAddress($addressActual, $addressUpdated);


            $propertyRooms = $request->request->get('property')['property-rooms'];
            $this->updatePropertyRooms($propertyRooms);

            if (isset($request->request->get('property')['rooms'])) {
                $this->addNewRooms($request->request->get('property')['rooms'], $property);
            }

            $propertyAmenities = $request->request->get('property')['property-amenities'];
            $this->updatePropertyAmenities($propertyAmenities);

            if (isset($request->request->get('property')['amenities'])) {
                $this->addNewAmenities($request->request->get('property')['amenities'], $property);
            }

            $manager->flush();

            return $this->redirectToRoute('property_show', array('id' => $property->getId()));
        }

        return $this->render('property/edit.html.twig', array(
            'property' => $property,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Property entity.
     *
     * @Route("/{id}", name="property_delete")
     * @Method("POST")
     *
     * @param Request  $request
     * @param Property $property
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, Property $property)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($property);
            $em->flush();

            return new JsonResponse('deleted', 200);
        }

        return new JsonResponse('Internal Sever Error', 500);
    }

    /**
     * @Route("/rooms", name="property_rooms")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRoomsAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $rooms = $manager->getRepository('AppBundle:Room')->findAll();

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $rooms,
                'formName' => 'property',
                'inputPrefix' => 'rooms',
                'inputType' => 'number'
            ]
        );
    }

    /**
     * @Route("/{id}/property-rooms", name="property_property_rooms")
     * @Method("GET")
     *
     * @param Property $property
     *
     * @return Response
     */
    public function getPropertyRoomsAction(Property $property)
    {
        $manager = $this->getDoctrine()->getManager();
        $propertyRooms = $manager->getRepository('AppBundle:PropertyRooms')->findBy(['property' => $property]);

        return $this->render(
            'widgets/multiple-select-update-form.html.twig',
            [
                'entities' => $propertyRooms,
                'formName' => 'property',
                'inputPrefix' => 'property-rooms',
                'inputType' => 'number',
                'attDescription' => 'room',
                'attValue' => 'quantity'
            ]
        );
    }

    /**
     *
     * @Route("/{id}/property-missing-rooms", name="property_missing_rooms")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return Response
     */
    public function getPropertyMissingRoomsAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $rooms = $manager->getRepository('AppBundle:Room')->getPropertyMissingRooms($id);

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $rooms->getQuery()->getResult(),
                'formName' => 'property',
                'inputPrefix' => 'rooms',
                'inputType' => 'number'
            ]
        );
    }

    /**
     * @param array $propertyRooms
     */
    private function updatePropertyRooms(array $propertyRooms)
    {
        $manager = $this->getDoctrine()->getManager();

        foreach ($propertyRooms as $id => $quantity) {
            $propertyR = $manager->getRepository('AppBundle:PropertyRooms')->find($id);

            if (empty($quantity) || $quantity == 0) {
                $manager->remove($propertyR);
            } else {
                $propertyR->setQuantity($quantity);
            }
        }
    }

    /**
     * @Route("/amenities", name="property_amenities")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAmenitiesAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $amenities = $manager->getRepository('AppBundle:Amenity')->findAll();

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $amenities,
                'formName' => 'property',
                'inputPrefix' => 'amenities',
                'inputType' => 'number'
            ]
        );
    }

    /**
     * @Route("/{id}/property-amenities", name="property_property_amenities")
     * @Method("GET")
     *
     * @param Property $property
     *
     * @return Response
     */
    public function getPropertyAmenitiesAction(Property $property)
    {
        $manager = $this->getDoctrine()->getManager();
        $propertyAmenities = $manager->getRepository('AppBundle:PropertyAmenities')->findBy(['property' => $property]);

        return $this->render(
            'widgets/multiple-select-update-form.html.twig',
            [
                'entities' => $propertyAmenities,
                'formName' => 'property',
                'inputPrefix' => 'property-amenities',
                'inputType' => 'number',
                'attDescription' => 'amenity',
                'attValue' => 'quantity'
            ]
        );
    }

    /**
     *
     * @Route("/{id}/property-missing-amenities", name="property_missing_amenities")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return Response
     */
    public function getPropertyMissingAmenitiesAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $amenities = $manager->getRepository('AppBundle:Amenity')->getPropertyMissingAmenities($id);

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $amenities->getQuery()->getResult(),
                'formName' => 'property',
                'inputPrefix' => 'amenities',
                'inputType' => 'number'
            ]
        );
    }

    /**
     * @param array $propertyAmenities
     */
    private function updatePropertyAmenities(array $propertyAmenities)
    {
        $manager = $this->getDoctrine()->getManager();

        foreach ($propertyAmenities as $id => $quantity) {
            $propertyA = $manager->getRepository('AppBundle:PropertyAmenities')->find($id);

            if (empty($quantity) || $quantity == 0) {
                $manager->remove($propertyA);
            } else {
                $propertyA->setQuantity($quantity);
            }
        }
    }

    /**
     * @return Response
     */
    public function getPropertiesAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $properties = count($manager->getRepository('AppBundle:Property')->findAll());

        return new Response($properties);
    }

    /**
     * @param array    $newRooms
     * @param Property $property
     */
    private function addNewRooms(array $newRooms, Property $property)
    {
        $manager = $this->getDoctrine()->getManager();

        foreach ($newRooms as $id => $quantity) {
            if ($quantity) {
                $room = $manager->getRepository('AppBundle:Room')->find($id);
                $propertyRooms = new PropertyRooms();
                $propertyRooms
                    ->setProperty($property)
                    ->setRoom($room)
                    ->setQuantity($quantity)
                ;

                $manager->persist($propertyRooms);
            }
        }
    }

    /**
     * @param array    $newAmenities
     * @param Property $property
     */
    private function addNewAmenities(array $newAmenities, Property $property)
    {
        $manager = $this->getDoctrine()->getManager();

        foreach ($newAmenities as $id => $quantity) {
            if ($quantity) {
                $amenity = $manager->getRepository('AppBundle:Amenity')->find($id);
                $propertyAmenities = new PropertyAmenities();
                $propertyAmenities
                    ->setAmenity($amenity)
                    ->setProperty($property)
                    ->setQuantity($quantity)
                ;

                $manager->persist($propertyAmenities);
            }
        }
    }

    /**
     * @param Address $actual
     * @param Address $new
     */
    private function updateAddress(Address $actual, Address $new)
    {
        $actual
            ->setAddress($new->getAddress())
            ->setCity($new->getCity())
            ->setZipCode($new->getZipCode())
            ->setState($new->getState())
            ->setCountry($new->getCountry())
        ;
    }
}
