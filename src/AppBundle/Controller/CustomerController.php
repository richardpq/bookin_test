<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\CustomerContacts;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Customer;
use AppBundle\Form\Type\CustomerType;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\ContactType;

/**
 * Customer controller.
 *
 * @Route("/customer")
 */
class CustomerController extends Controller
{
    /**
     * Lists all Customer entities.
     *
     * @Route("/", name="customer_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $customers = $em->getRepository('AppBundle:Customer')->findAll();

        return $this->render('customer/index.html.twig', array(
            'customers' => $customers,
        ));
    }

    /**
     * Creates a new Customer entity.
     *
     * @Route("/new", name="customer_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        //TODO: create handler for address
        //TODO: create function for handle contact details
        $customer = new Customer();
        $form = $this->createForm('AppBundle\Form\Type\CustomerType', $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->get('address')->getData();
            $manager = $this->getDoctrine()->getManager();

            if ($address === null && $form->get('customerAddress')) {
                $address = $form->get('customerAddress')->getData();

                $manager->persist($address);
                $manager->flush();
            }

            $customer->setAddress($address);
            $manager->persist($customer);
            $manager->flush();

            $contactDetails = $request->request->get('customer')['contacts'];

            $this->addNewContacts($contactDetails, $customer);
            $manager->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customer/new.html.twig', array(
            'customer' => $customer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Customer entity.
     *
     * @Route("/{id}/edit", name="customer_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request  $request
     * @param Customer $customer
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, Customer $customer)
    {
        //TODO: create handler for address
        //TODO: create function for handle contact details
        $manager = $this->getDoctrine()->getManager();
        $address = $manager->getRepository('AppBundle:Address')->find($customer->getAddress());
        $manager->initializeObject($address);
        $editForm = $this->createForm(
            'AppBundle\Form\Type\CustomerType',
            $customer,
            ['address' => $address]
        );

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $addressActual = $editForm->get('address')->getData();
            $addressUpdated = $editForm->get('customerAddress')->getData();

            $this->updateAddress($addressActual, $addressUpdated);

            $customerContacts = $request->request->get('customer')['customer-contacts'];
            $this->updateCustomerContacts($customerContacts);

            if (isset($request->request->get('customer')['contacts'])) {
                $this->addNewContacts($request->request->get('customer')['contacts'], $customer);
            }

            $manager->flush();


            return $this->redirectToRoute('customer_index', array('id' => $customer->getId()));
        }

        return $this->render('customer/edit.html.twig', array(
            'customer' => $customer,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Customer entity.
     *
     * @Route("/{id}", name="customer_delete")
     * @Method("POST")
     *
     * @param Request  $request
     * @param Customer $customer
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, Customer $customer)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($customer);
            $em->flush();

            return new JsonResponse('deleted', 200);
        }

        return new JsonResponse('Internal Sever Error', 500);
    }

    /**
     * @return Response
     */
    public function getCustomersAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $customers = count($manager->getRepository('AppBundle:Customer')->findAll());

        return new Response($customers);
    }

    /**
     * @Route("/contacts", name="customer_contacts")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getContactsAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $contacts = $manager->getRepository('AppBundle:ContactType')->findAll();

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $contacts,
                'formName' => 'customer',
                'inputPrefix' => 'contacts',
                'inputType' => 'text'
            ]
        );
    }

    /**
     * @Route("/{id}/customer-contacts", name="customer_customer_contacts")
     * @Method("GET")
     *
     * @param Customer $customer
     *
     * @return Response
     */
    public function getCustomerContactsAction(Customer $customer)
    {
        $manager = $this->getDoctrine()->getManager();
        $contacts = $manager->getRepository('AppBundle:CustomerContacts')->findBy(['customer' => $customer]);

        return $this->render(
            'widgets/multiple-select-update-form.html.twig',
            [
                'entities' => $contacts,
                'formName' => 'customer',
                'inputPrefix' => 'customer-contacts',
                'inputType' => 'text',
                'attDescription' => 'contactType',
                'attValue' => 'contact'
            ]
        );
    }

    /**
     *
     * @Route("/{id}/customer-missing-contacts", name="customer_missing_contacts")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return Response
     */
    public function getCustomerMissingContactsAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $contacts = $manager->getRepository('AppBundle:ContactType')->getCustomerMissingContacts($id);

        return $this->render(
            'widgets/multiple-select-input.html.twig',
            [
                'entities' => $contacts->getQuery()->getResult(),
                'formName' => 'customer',
                'inputPrefix' => 'contacts',
                'inputType' => 'text'
            ]
        );
    }

    /**
     * @param Address $actual
     * @param Address $new
     */
    private function updateAddress(Address $actual, Address $new)
    {
        $actual
            ->setAddress($new->getAddress())
            ->setCity($new->getCity())
            ->setZipCode($new->getZipCode())
            ->setState($new->getState())
            ->setCountry($new->getCountry())
        ;
    }

    /**
     * @param array $customerContacts
     */
    private function updateCustomerContacts(array $customerContacts)
    {
        $manager = $this->getDoctrine()->getManager();

        foreach ($customerContacts as $id => $customerContact) {
            $contact = $manager->getRepository('AppBundle:CustomerContacts')->find($id);

            if (empty(trim($customerContact))) {
                $manager->remove($contact);
            } else {
                $contact->setContact(trim($customerContact));
            }
        }
    }

    /**
     * @param array    $newContacts
     * @param Customer $customer
     */
    private function addNewContacts(array $newContacts, Customer $customer)
    {
        $manager = $this->getDoctrine()->getManager();
        foreach ($newContacts as $id => $contact) {
            if ($contact) {
                $contactType = $manager->getRepository('AppBundle:ContactType')->find($id);
                $customerContacts = new CustomerContacts();
                $customerContacts
                    ->setCustomer($customer)
                    ->setContactType($contactType)
                    ->setContact($contact)
                ;
                $manager->persist($customerContacts);
            }
        }
    }
}
