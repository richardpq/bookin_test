<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\PropertyType;

/**
 * PropertyType controller.
 *
 * @Route("/property-type")
 */
class PropertyTypeController extends Controller
{
    /**
     * Lists all PropertyType entities.
     *
     * @Route("/", name="property-type_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $propertyTypes = $em->getRepository('AppBundle:PropertyType')->findAll();

        return $this->render('propertytype/index.html.twig', array(
            'propertyTypes' => $propertyTypes,
        ));
    }

    /**
     * Creates a new PropertyType entity.
     *
     * @Route("/new", name="property-type_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $propertyType = new PropertyType();
        $form = $this->createForm('AppBundle\Form\Type\PropertyTypeType', $propertyType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($propertyType);
            $em->flush();

            return $this->redirectToRoute('property-type_index');
        }

        return $this->render('propertytype/new.html.twig', array(
            'propertyType' => $propertyType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PropertyType entity.
     *
     * @Route("/{id}/edit", name="property-type_edit")
     * @Method({"GET", "POST"})
     *
     *
     * @param Request      $request
     * @param PropertyType $propertyType
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, PropertyType $propertyType)
    {
        $editForm = $this->createForm('AppBundle\Form\Type\PropertyTypeType', $propertyType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($propertyType);
            $em->flush();

            return $this->redirectToRoute('property-type_edit', array('id' => $propertyType->getId()));
        }

        return $this->render('propertytype/edit.html.twig', array(
            'propertyType' => $propertyType,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a PropertyType entity.
     *
     * @Route("/{id}", name="property-type_delete")
     * @Method("POST")
     *
     * @param Request      $request
     * @param PropertyType $propertyType
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, PropertyType $propertyType)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($propertyType);
            $em->flush();

            return new JsonResponse('deleted', 200);
        }

        return new JsonResponse('Internal Sever Error', 500);
    }
}
