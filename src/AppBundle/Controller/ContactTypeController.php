<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\ContactType;

/**
 * ContactType controller.
 *
 * @Route("/contact-type")
 */
class ContactTypeController extends Controller
{
    /**
     * Lists all ContactType entities.
     *
     * @Route("/", name="contact-type_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contactTypes = $em->getRepository('AppBundle:ContactType')->findAll();

        return $this->render('contacttype/index.html.twig', array(
            'contactTypes' => $contactTypes,
        ));
    }

    /**
     * Creates a new ContactType entity.
     *
     * @Route("/new", name="contact-type_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $contactType = new ContactType();
        $form = $this->createForm('AppBundle\Form\Type\ContactTypeType', $contactType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactType);
            $em->flush();

            return $this->redirectToRoute('contact-type_index');
        }

        return $this->render('contacttype/new.html.twig', array(
            'contactType' => $contactType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ContactType entity.
     *
     * @Route("/{id}/edit", name="contact-type_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request     $request
     * @param ContactType $contactType
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, ContactType $contactType)
    {
        $editForm = $this->createForm('AppBundle\Form\Type\ContactTypeType', $contactType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactType);
            $em->flush();

            return $this->redirectToRoute('contact-type_edit', array('id' => $contactType->getId()));
        }

        return $this->render('contacttype/edit.html.twig', array(
            'contactType' => $contactType,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a ContactType entity.
     *
     * @Route("/{id}", name="contact-type_delete")
     * @Method("POST")
     *
     * @param Request     $request
     * @param ContactType $contactType
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, ContactType $contactType)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contactType);
            $em->flush();

            return new JsonResponse('deleted', 200);
        }

        return new JsonResponse('Internal Sever Error', 500);
    }
}
