<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RoomRepository extends EntityRepository
{
    /**
     * @param integer $id property Id
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPropertyMissingRooms($id)
    {
        $manager = $this->getEntityManager();
        $sub = $manager->createQueryBuilder();
        $sub
            ->select('DISTINCT(pr.room)')
            ->from('AppBundle:PropertyRooms', 'pr')
            ->join('pr.property', 'pt', 'pt.id = pr.property_id')
            ->where("pt.id = $id")
        ;

        $rqb = $manager->createQueryBuilder();
        $rqb
            ->select('r')
            ->from('AppBundle:Room', 'r')
            ->where($rqb->expr()->notIn('r.id', $sub->getDQL()))
            ->getQuery()
        ;

        return $rqb;
    }
}
