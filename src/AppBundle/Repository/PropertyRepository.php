<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PropertyRepository extends EntityRepository
{
    public function getPropertyById($id)
    {
        $manager = $this->getEntityManager();

        $qb = $manager->createQueryBuilder();

        $qb
            ->from('AppBundle:Property', 'p')
            ->join('p.address', 'pad')
            ->join('p.propertyRooms', 'pr')
            ->join('p.propertyAmenities', 'pa')
            ->join('p.propertyType', 'pt')
            ->join('pr.room', 'r')
            ->select('p, pad, pr, pa, pt, r')
            ->where('p.id =:id')->setParameter('id', $id);

        return $qb->getQuery()->getSingleResult();
    }
}
