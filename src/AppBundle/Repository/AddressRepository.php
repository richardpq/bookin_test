<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AddressRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCustomerAddress()
    {
        $manager = $this->getEntityManager();
        $pqb = $manager->createQueryBuilder();
        $pqb
            ->select('DISTINCT(a.id)')
            ->from('AppBundle:Address', 'a')
            ->innerJoin('a.properties', 'p')
        ;

        $cqb = $manager->createQueryBuilder();
        $cqb
            ->select('ad')
            ->from('AppBundle:Address', 'ad')
            ->where($cqb->expr()->notIn('ad.id', $pqb->getDQL()))
            ->getQuery()
        ;

        return $cqb;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPropertyAddress()
    {
        $manager = $this->getEntityManager();
        $cqb = $manager->createQueryBuilder();
        $cqb
            ->select('DISTINCT(a.id)')
            ->from('AppBundle:Address', 'a')
            ->innerJoin('a.customers', 'c')
        ;

        $pqb = $manager->createQueryBuilder();
        $pqb
            ->select('ad')
            ->from('AppBundle:Address', 'ad')
            ->where($pqb->expr()->notIn('ad.id', $cqb->getDQL()))
            ->getQuery()
        ;

        return $pqb;
    }
}
