<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ContactTypeRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCustomerMissingContacts($id)
    {
        $manager = $this->getEntityManager();
        $sub = $manager->createQueryBuilder();
        $sub
            ->select('DISTINCT(cc.contactType)')
            ->from('AppBundle:CustomerContacts', 'cc')
            ->join('cc.customer', 'cus', 'cus.id = cc.customer_id')
            ->where("cus.id = $id")
        ;

        $cqb = $manager->createQueryBuilder();
        $cqb
            ->select('ct')
            ->from('AppBundle:ContactType', 'ct')
            ->where($cqb->expr()->notIn('ct.id', $sub->getDQL()))
            ->getQuery()
        ;

        return $cqb;
    }
}
