<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AmenityRepository extends  EntityRepository
{
    /**
     * @param integer $id property Id
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPropertyMissingAmenities($id)
    {
        $manager = $this->getEntityManager();
        $sub = $manager->createQueryBuilder();
        $sub
            ->select('DISTINCT(pa.amenity)')
            ->from('AppBundle:PropertyAmenities', 'pa')
            ->join('pa.property', 'pt', 'pt.id = pr.property_id')
            ->where("pt.id = $id")
        ;

        $aqb = $manager->createQueryBuilder();
        $aqb
            ->select('a')
            ->from('AppBundle:Amenity', 'a')
            ->where($aqb->expr()->notIn('a.id', $sub->getDQL()))
            ->getQuery()
        ;

        return $aqb;
    }

    public function getAll()
    {
        $manager = $this->getEntityManager();

        $qb = $manager->createQueryBuilder();

        $qb
            ->from('AppBundle:Amenity', 'a')
            ->select('a.id, a.description')
        ;

        return $qb->getQuery()->getArrayResult();
    }
}
