<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\AddressRepository;
use AppBundle\Repository\AmenityRepository;
use AppBundle\Repository\RoomRepository;

class PropertyType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class)
            ->add('maxPeople')
            ->add('checkIn', TimeType::class)
            ->add('checkOut', TimeType::class)
            ->add('parkingSpots')
            ->add('propertyType')
            ->add('address', EntityType::class, [
                'class' => 'AppBundle:Address',
                'placeholder' => 'Create new Address',
                'required' => false,
                'query_builder' => function (AddressRepository $er) {
                    return $er->getPropertyAddress();
                },
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $property = $event->getData();
            $form = $event->getForm();
            $id = $property->getId();

            if (!$id) {
                $form
                    ->add('room', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:Room',
                        'label' => 'Rooms',
                        'multiple' => true
                    ])
                    ->add('amenity', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:Amenity',
                        'label' => 'Amenities',
                        'multiple' => true
                    ])
                    ->add('propertyAddress', AddressType::class, ['mapped' => false])
                ;
            } else {
                $form
                    ->add('room', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:Room',
                        'label' => 'Rooms',
                        'multiple' => true,
                        'required' => false,
                        'query_builder' => function (RoomRepository $er) use ($id) {
                            return $er->getPropertyMissingRooms($id);
                        },
                    ])
                    ->add('amenity', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:Amenity',
                        'label' => 'Amenities',
                        'multiple' => true,
                        'required' => false,
                        'query_builder' => function (AmenityRepository $er) use ($id) {
                            return $er->getPropertyMissingAmenities($id);
                        },
                    ])
                    ->add('propertyAddress', AddressType::class, ['mapped' => false, 'data' => $options['address']])
                ;
            }
        });
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Property',
            'allow_extra_fields' => true,
            'address' => null
        ));
    }
}
