<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\AddressRepository;
use AppBundle\Repository\ContactTypeRepository;

class CustomerType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('address', EntityType::class, [
                'class' => 'AppBundle:Address',
                'placeholder' => 'Create new Address',
                'required' => false,
                'query_builder' => function (AddressRepository $er) {
                    return $er->getCustomerAddress();
                },
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $customer = $event->getData();
            $form = $event->getForm();
            $id = $customer->getId();
            if (!$id) {
                $form
                    ->add('contact', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:ContactType',
                        'label' => 'Contacts',
                        'multiple' => true
                    ])
                    ->add('customerAddress', AddressType::class, ['mapped' => false])
                ;
            } else {
                $form
                    ->add('contact', EntityType::class, [
                        'mapped' => false,
                        'class' => 'AppBundle:ContactType',
                        'label' => 'Contacts',
                        'multiple' => true,
                        'required' => false,
                        'query_builder' => function (ContactTypeRepository $er) use ($id) {
                            return $er->getCustomerMissingContacts($id);
                        },
                    ])
                    ->add('customerAddress', AddressType::class, ['mapped' => false, 'data' => $options['address']])
                ;
            }
        });
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Customer',
            'allow_extra_fields' => true,
            'address' => null
        ));
    }
}
