<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Room;

/**
 * Class Load30RoomType
 * @package AppBundle\DataFixtures\ORM
 */
class Load30Room extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $data = [
            'Bedroom',
            'Full Kitchen',
            'Kitchenette',
            'Game Room',
            'Laundry',
            'Living Room',
            'Dinning Room',
            'Bath Room'
        ];

        foreach ($data as $room) {
            $roomType = new Room();
            $roomType->setDescription($room);

            $manager->persist($roomType);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 30;
    }
}
