<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\PropertyType;

/**
 * Class Load10PropertyType
 * @package AppBundle\DataFixtures\ORM
 */
class Load10PropertyType extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $data = ['Room', 'Apartment', 'House'];

        foreach ($data as $propertyType) {
            $property = new PropertyType();
            $property->setType($propertyType);

            $manager->persist($property);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 10;
    }
}
