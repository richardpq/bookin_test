<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Amenity;

/**
 * Class Load20Amenity
 * @package AppBundle\DataFixtures\ORM
 */
class Load20Amenity extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $data = ['WiFi', 'Tv 32"', 'Wired Internet', 'Tv Cable', 'Iron', 'Dish Washer', 'Netflix'];

        foreach ($data as $amenityDescription) {
            $amenity = new Amenity();
            $amenity->setDescription($amenityDescription);

            $manager->persist($amenity);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 20;
    }
}
