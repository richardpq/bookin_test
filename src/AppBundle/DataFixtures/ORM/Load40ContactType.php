<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\ContactType;

/**
 * Class Load40ContactType
 * @package AppBundle\DataFixtures\ORM
 */
class Load40ContactType extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $data = ['Mobile', 'Day Phone', 'Skype', 'Twitter', 'Email'];

        foreach ($data as $contact) {
            $contactType = new ContactType();
            $contactType->setType($contact);

            $manager->persist($contactType);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 40;
    }
}
