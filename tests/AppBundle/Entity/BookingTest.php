<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Booking;
use AppBundle\Entity\Property;
use AppBundle\Entity\Customer;

class BookingTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $booking = new Booking();
        $booking
            ->setCheckIn(new \DateTime('2016-01-01'))
            ->setCheckOut(new \DateTime('2016-01-11'))
            ->setRatePerNight('90.52')
            ->setCustomerArrivedDate(new \DateTime('2016-01-01 18:00:00'))
            ->setCustomerDepartedDate(new \DateTime('2016-01-11 10:00:00'))
            ->setCustomer(new Customer())
            ->setProperty(new Property())
            ;

        $this->assertEquals('2016-01-01', $booking->getCheckIn()->format('Y-m-d'));
        $this->assertEquals('2016-01-11', $booking->getCheckOut()->format('Y-m-d'));
        $this->assertEquals(90.52, $booking->getRatePerNight());
        $this->assertEquals('2016-01-01 18:00:00', $booking->getCustomerArrivedDate()->format('Y-m-d H:i:s'));
        $this->assertEquals('2016-01-11 10:00:00', $booking->getCustomerDepartedDate()->format('Y-m-d H:i:s'));
        $this->assertEquals(new Customer(), $booking->getCustomer());
        $this->assertEquals(new Property(), $booking->getProperty());
        $this->assertEquals(10, $booking->getTotalBookingDays());
        $this->assertEquals(905.2, $booking->getTotalAmountBooking());
    }

    /**
     * @expectedException \Exception
     */
    public function testExceptionForBookingDaysWhenDatesAreNotSet()
    {
        $booking = new Booking();
        $booking->getTotalBookingDays();
    }

    /**
     * @expectedException \Exception
     */
    public function testExceptionForTotalAmountBookingWhenDatesAreNotSet()
    {
        $booking = new Booking();
        $booking->getTotalAmountBooking();
    }
}
