<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Room;


class RoomTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $room = new Room();
        $room
            ->setDescription('Bedroom')
        ;

        $this->assertEquals('Bedroom', $room->getDescription());

    }
}
