<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\ContactType;

class ContactTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testContactType()
    {
        $contact = new ContactType();
        $contact->setType('Mobile');

        $this->assertEquals('Mobile', $contact->getType());
    }
}
