<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Address;
use AppBundle\Entity\Property;

class AddressTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $address = new Address();
        $address
            ->setAddress('1234 MyTest St')
            ->setCity('MyCity')
            ->setState('MS')
            ->setCountry('MyCountry')
            ->setZipCode('123456')
            ;

        $this->assertEquals('1234 MyTest St', $address->getAddress());
        $this->assertEquals('MyCity', $address->getCity());
        $this->assertEquals('MS', $address->getState());
        $this->assertEquals('MyCountry', $address->getCountry());
        $this->assertEquals('123456', $address->getZipCode());
        $this->assertEquals("1234 MyTest St\nMyCity, MS 123456\nMyCountry", $address);
    }

    public function testAddRemoveProperty()
    {
        $address = new Address();

        $property1 = new Property();
        $property1->setName('One');

        $property2 = new Property();
        $property2->setName('Two');

        $property3 = new Property();
        $property3->setName('Three');

        $address
            ->addProperty($property1)
            ->addProperty($property2)
            ->addProperty($property3)
        ;

        $this->assertCount(3, $address->getProperties());
        $this->assertTrue($address->getProperties()->contains($property1));

        $address->removeProperty($property2);

        $this->assertCount(2, $address->getProperties());
        $this->assertFalse($address->getProperties()->contains($property2));
    }
}
