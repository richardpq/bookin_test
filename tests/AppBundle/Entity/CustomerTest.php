<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Address;
use AppBundle\Entity\CustomerContacts;
use AppBundle\Entity\Booking;

class CustomerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Customer $customer */
    protected $customer;

    public function setUp()
    {
        $this->customer = new Customer('Richard', 'Perez');
    }

    public function testAccessors()
    {
        $this->customer->setAddress(new Address());

        $this->assertEquals('Richard Perez', $this->customer);
        $this->assertEquals(new Address(), $this->customer->getAddress());
    }

    public function testAddRemoveBooking()
    {
        $booking1 = new Booking();
        $booking1->setRatePerNight('10');

        $booking2 = new Booking();
        $booking2->setRatePerNight('20');

        $booking3 = new Booking();
        $booking3->setRatePerNight('30');

        $this->customer
            ->addBooking($booking1)
            ->addBooking($booking2)
            ->addBooking($booking3)
        ;

        $this->assertCount(3, $this->customer->getBookings());
        $this->assertTrue($this->customer->getBookings()->contains($booking2));
        $this->customer->removeBooking($booking2);
        $this->assertFalse($this->customer->getBookings()->contains($booking2));
    }

    public function testAddRemoveCustomerContacts()
    {
        $customerContact1 = new CustomerContacts();
        $customerContact1->setContact('customer1@gmail.com');

        $customerContact2 = new CustomerContacts();
        $customerContact2->setContact('customer2@gmail.com');

        $customerContact3 = new CustomerContacts();
        $customerContact3->setContact('customer2@gmail.com');

        $this->customer
            ->addCustomerContact($customerContact1)
            ->addCustomerContact($customerContact2)
            ->addCustomerContact($customerContact3)
        ;

        $this->assertCount(3, $this->customer->getCustomerContacts());
        $this->assertTrue($this->customer->getCustomerContacts()->contains($customerContact2));
        $this->customer->removeCustomerContact($customerContact2);
        $this->assertFalse($this->customer->getCustomerContacts()->contains($customerContact2));
    }
}
