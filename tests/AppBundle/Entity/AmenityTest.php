<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Amenity;

class AmenityTest extends \PHPUnit_Framework_TestCase
{
    public function testAmenityDescription()
    {
        $amenity = new Amenity();
        $amenity->setDescription('Wifi');

        $this->assertEquals('Wifi', $amenity->getDescription());
    }
}
