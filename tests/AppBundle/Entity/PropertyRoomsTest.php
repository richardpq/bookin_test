<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\PropertyRooms;
use AppBundle\Entity\Property;
use AppBundle\Entity\Room;

class PropertyRoomsTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $propertyRooms = new PropertyRooms();
        $propertyRooms
            ->setQuantity(10)
            ->setProperty(new Property())
            ->setRoom(new Room())
        ;

        $this->assertEquals(10, $propertyRooms->getQuantity());
        $this->assertEquals(new Property(), $propertyRooms->getProperty());
        $this->assertEquals(new Room(), $propertyRooms->getRoom());
    }
}
