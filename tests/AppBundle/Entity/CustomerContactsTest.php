<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\CustomerContacts;
use AppBundle\Entity\Customer;
use AppBundle\Entity\ContactType;

class CustomerContactsTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $customerContacts = new CustomerContacts();
        $customerContacts
            ->setContact('customer@gmail.com')
            ->setContactType(new ContactType())
            ->setCustomer(new Customer())
        ;

        $this->assertEquals('customer@gmail.com', $customerContacts->getContact());
        $this->assertEquals(new ContactType(), $customerContacts->getContactType());
        $this->assertEquals(new Customer(), $customerContacts->getCustomer());
    }
}
