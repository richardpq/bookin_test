<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Person;

class PersonTest extends \PHPUnit_Framework_TestCase
{
    public function testNameAndLastName()
    {
        $person = new Person();
        $person
            ->setFirstName('John')
            ->setLastName('Doe')
            ;

        $this->assertEquals('John', $person->getFirstName());
        $this->assertEquals('Doe', $person->getLastName());
        $this->assertEquals('John Doe', $person);
    }
}
