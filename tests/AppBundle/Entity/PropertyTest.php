<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Address;
use AppBundle\Entity\Property;
use AppBundle\Entity\PropertyType;
use AppBundle\Entity\PropertyRooms;
use AppBundle\Entity\PropertyAmenities;
use AppBundle\Entity\Booking;

class PropertyTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Property */
    protected $property;
    
    public function setUp()
    {
        $this->property = new Property();
    }
    
    public function testAccessors()
    {
        $checkIn = new \DateTime();
        $checkIn->setTime(16, 0);

        $checkOut = new \DateTime();
        $checkOut->setTime(11, 00);

        $this->property
            ->setName('Vancouver Summer House, Basement 1')
            ->setDescription('Two Basement house on Cree St')
            ->setCheckIn($checkIn)
            ->setCheckOut($checkOut)
            ->setMaxPeople(2)
            ->setParkingSpots(1)
            ->setAddress(new Address())
            ->setPropertyType(new PropertyType())
        ;

        $this->assertEquals('Vancouver Summer House, Basement 1', $this->property);
        $this->assertEquals('Vancouver Summer House, Basement 1', $this->property->getName());
        $this->assertEquals('Two Basement house on Cree St', $this->property->getDescription());
        $this->assertEquals(2, $this->property->getMaxPeople());
        $this->assertEquals(1, $this->property->getParkingSpots());
        $this->assertEquals(new Address(), $this->property->getAddress());
        $this->assertEquals(new PropertyType(), $this->property->getPropertyType());
        $this->assertEquals('16:00', $this->property->getCheckIn()->format('H:i'));
        $this->assertEquals('11:00', $this->property->getCheckOut()->format('H:i'));
    }

    public function testAddRemovePropertyRooms()
    {
        $propertyRoom1 = new PropertyRooms();
        $propertyRoom1->setQuantity(1);

        $propertyRoom2 = new PropertyRooms();
        $propertyRoom2->setQuantity(2);

        $propertyRoom3 = new PropertyRooms();
        $propertyRoom3->setQuantity(2);

        $this->property
            ->addPropertyRoom($propertyRoom1)
            ->addPropertyRoom($propertyRoom2)
            ->addPropertyRoom($propertyRoom3)
        ;

        $this->assertCount(3, $this->property->getPropertyRooms());
        $this->assertTrue($this->property->getPropertyRooms()->contains($propertyRoom2));
        $this->property->removePropertyRoom($propertyRoom2);
        $this->assertFalse($this->property->getPropertyRooms()->contains($propertyRoom2));
    }

    public function testAddRemovePropertyAmenities()
    {
        $propertyAmenity1 = new PropertyAmenities();
        $propertyAmenity1->setQuantity(1);

        $propertyAmenity2 = new PropertyAmenities();
        $propertyAmenity2->setQuantity(2);

        $propertyAmenity3 = new PropertyAmenities();
        $propertyAmenity3->setQuantity(2);

        $this->property
            ->addPropertyAmenity($propertyAmenity1)
            ->addPropertyAmenity($propertyAmenity2)
            ->addPropertyAmenity($propertyAmenity3)
        ;

        $this->assertCount(3, $this->property->getPropertyAmenities());
        $this->assertTrue($this->property->getPropertyAmenities()->contains($propertyAmenity2));
        $this->property->removePropertyAmenity($propertyAmenity2);
        $this->assertFalse($this->property->getPropertyAmenities()->contains($propertyAmenity2));
    }

    public function testAddRemoveBookings()
    {
        $booking1 = new Booking();
        $booking1->setRatePerNight('10');

        $booking2 = new Booking();
        $booking2->setRatePerNight('20');

        $booking3 = new Booking();
        $booking3->setRatePerNight('30');

        $this->property
            ->addBooking($booking1)
            ->addBooking($booking2)
            ->addBooking($booking3)
        ;

        $this->assertCount(3, $this->property->getBookings());
        $this->assertTrue($this->property->getBookings()->contains($booking2));
        $this->property->removeBooking($booking2);
        $this->assertFalse($this->property->getBookings()->contains($booking2));
    }
}
