<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\PropertyAmenities;
use AppBundle\Entity\Property;
use AppBundle\Entity\Amenity;

class PropertyAmenitiesTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessors()
    {
        $propertyAmenities = new PropertyAmenities();
        $propertyAmenities
            ->setQuantity(10)
            ->setAmenity(new Amenity())
            ->setProperty(new Property())
        ;

        $this->assertEquals(10, $propertyAmenities->getQuantity());
        $this->assertEquals(new Amenity(), $propertyAmenities->getAmenity());
        $this->assertEquals(new Property(), $propertyAmenities->getProperty());
    }
}
