<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\PropertyType;
use AppBundle\Entity\Property;

class PropertyTypeTest extends \PHPUnit_Framework_TestCase
{
    /** @var  PropertyType $propertyType */
    protected $propertyType;

    public function setUp()
    {
        $propertyType = new PropertyType();
        $propertyType->setType('House');

        $this->propertyType = $propertyType;
    }

    public function testPropertyType()
    {
        $this->assertEquals('House', $this->propertyType->getType());
    }

    public function testAddRemoveProperty()
    {
        $property1 = new Property();
        $property1->setName('One');

        $property2 = new Property();
        $property2->setName('Two');

        $property3 = new Property();
        $property3->setName('Three');

        $this->propertyType
            ->addProperty($property1)
            ->addProperty($property2)
            ->addProperty($property3)
        ;

        $this->assertCount(3, $this->propertyType->getProperties());
        $this->assertTrue($this->propertyType->getProperties()->contains($property1));

        $this->propertyType->removeProperty($property2);

        $this->assertCount(2, $this->propertyType->getProperties());
        $this->assertFalse($this->propertyType->getProperties()->contains($property2));
    }
}
